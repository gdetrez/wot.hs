{-# LANGUAGE OverloadedStrings #-}

import Test.Hspec

import Data.Attoparsec.Text

import Parser

main :: IO ()
main = hspec $ do
  describe "Parser.keyParser" $
    it "parses a key without identities or subkeys" $
      parseOnly keyParser "pub:-:1024:17:DB26302481DDB065:1160261964:::-:::scESC::::::"
      `shouldBe` Right (Key "DB26302481DDB065" [])
