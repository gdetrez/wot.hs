module Main where

import Data.Attoparsec.ByteString
import Data.ByteString
import Prelude hiding (getContents)

import Parser

main :: IO ()
main = parseOnly gpgParser <$> getContents >>= print
-- main = TIO.getContents >>= print
