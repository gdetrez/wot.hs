{-# LANGUAGE OverloadedStrings #-}
module Parser where

import Control.Applicative
import Data.Maybe
import qualified Data.ByteString as BS
import qualified Data.ByteString.Char8 as C
import Prelude hiding (takeWhile)

-- import Data.Attoparsec.ByteString
import Data.Attoparsec.ByteString.Char8





data Key = Key
    { keyValidity :: Validity
    , keyLength :: Integer
    , keyAlgorithm :: Integer
    , keyId :: Keyid
    , keyUserIds :: [(Uid, [Sig])]
    }
  deriving (Eq, Show)

type Keyid = BS.ByteString
type Uid = String

data Sig = Sig { from :: Keyid }
  deriving (Eq, Show)

gpgParser :: Parser [Key]
gpgParser = skipLine >> many1 keyParser

skipLine :: Parser ()
skipLine = skipWhile (/= '\n') >> endOfLine

keyParser :: Parser Key

-- catMaybes <$>
-- 
--   ((Just <$> keyParser)
--     <|>
--   (skipWhile (not . isEndOfLine) >> return Nothing)) `sepBy` endOfLine

keyParser = do
    string "pub" <* colon
    val <- f validity
    len <- f decimal
    pka <- f decimal
    kid <- Data.Attoparsec.ByteString.Char8.takeWhile (inClass "abcdefABCDEF0123456789")
    skipLine
    many (("uat" <|> "fpr" <|> "sub") >> skipLine)
    uids <- many useridParser
    many (("uat" <|> "fpr" <|> "sub" <|> "sig") >> skipLine)
    return (Key val len pka kid uids)
--     string "pub"
--     colon
--     validity >> colon
--     skip >> colon
--     skip >> colon
--     kid <- takeWhile (inClass "abcdefABCDEF0123456789")
--     skipWhile (not . isEndOfLine)
--     -- endOfLine
--     return (Key kid [] )
  where
    f p = p <* colon
    colon = char ':'
skip = skipMany (notChar ':')

useridParser :: Parser (Uid, [Sig])
useridParser = do
    "uid"
    sep
    validity
    sep >> sep >> sep >> sep
    str
    sep >> sep
    hexadecimal :: Parser Int
    sep >> sep
    user <- str
    skipLine
    sigs <- catMaybes <$> many ((Just <$> sigParser) <|> ("rev" >> skipLine >> return Nothing))
    return (user, sigs)
    --sep
    -- endOfLine
    -- return user
  where
    sep = char ':'

str = many (notChar ':')

sep = char ':'

sigParser :: Parser Sig
sigParser = do
    "sig"
    sep >> sep >> sep
    decimal
    sep
    kid <- str
    sep
    str
    sep >> sep >> sep
    str
    skipLine
    return (Sig (C.pack kid))

data Validity = Invalid | NotValid | Marginal | Full | Ultimate
  deriving (Show, Eq, Ord)

validity :: Parser Validity
validity =
    (choice (Prelude.map char "oidre-qws") >> return Invalid)
    <|> (char 'n' >> return NotValid)
    <|> (char 'm' >> return Marginal)
    <|> (char 'f' >> return Full)
    <|> (char 'u' >> return Ultimate)
    <?> "Can't parse validity"

  -- pubParser :: Parser Keyid
  -- uidParser :: Parser Uid
  -- sigParser :: Parser Sig

